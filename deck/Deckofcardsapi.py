import requests
from deck.IDeck import IDeck


class Deckofcardsapi(IDeck):
    """
    Колода с https://deckofcardsapi.com/
    """
    def __init__(self):
        self.deck_id = ''

    def get_full_deck(self):
        url = 'https://deckofcardsapi.com/api/deck/new/shuffle/'
        param = {'deck_count': '6'}

        request = requests.get(url, params=param)
        data = request.json()
        try:
            self.deck_id = data['deck_id']
            print('Новая колода создана')
        except Exception as e:
            print('Не созданно id')
            print(e)

    def get_cart(self):
        try:
            param = {'count': '1'}
            url = f'https://deckofcardsapi.com/api/deck/{self.deck_id}/draw/'
            request = requests.get(url, params=param)
            data = request.json()
            return data
        except Exception as e:
            print('Ошибка при получении карты')
            print(e)

        # принт заменить на логи