from abc import ABC, abstractmethod


class IDeck(ABC):


    """
    Интерфейс колоды
    """


@abstractmethod
def get_full_deck(self):
    """
    Получить колоду
    :param self:
    :return:
    """
    pass


@abstractmethod
def get_cart(self):
    """
    Получить карту
    :param self:
    :return:
    """
    pass
