class Bank:

    def __init__(self, coin):
        self.coin = coin

    def add_coin(self, coin):
        self.coin = self.coin + coin

    def get_coin(self):
        return self.coin

    def send_coin(self, coin):
        self.coin = self.coin - coin

    def check_coin_send(self, coin):
        if self.coin >= coin:
            return True
        else:
            return False
