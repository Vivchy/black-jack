from blackjack_core.game import Game


class Bot:
    """
    бот получает 2 карты и смотрит что у него выпало
    если выпало меньше 15, то бот тянет еще 1 карту
    если больше 15 то бот оставанливается

     to do бот должен уметь делать ставки
    """

    def __init__(self):
        self.game = Game()
        self.game_log = ''

    def start(self):
        """
        Команда для запуска бота
        :return:
        """
        return 'start'

    def stop(self):
        """
        команда для осатновки бота
        :return:
        """
        return 'stop'

    def continuum(self):
        """
        команда для продолжения
        :return:
        """
        return 'continuum'

    def play(self, card):
        """
        Бот набирает карты в цикле пока не наберет больше 15
        :param card:
        :return:
        """

        self.game_log = self.game_log + f' {self.game.draw(card)}'
        if self.game.get_count() > 15:
            return self.stop()
        else:
            return self.continuum()

    def get_count(self):
        """
        Получить очки бота
        :return:
        """
        return self.game.get_count()

    def info(self):
        return self.game_log
