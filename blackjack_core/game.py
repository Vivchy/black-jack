class Game:
    def __init__(self):
        self.count = 0

    def play(self, cart_count):

        self.count = self.count + cart_count
        if self.count > 21:
            return self.lose()
        elif self.count == 21:
            return self.win()
        else:
            print(f'{self.count} - очки')

    def win(self):
        self.restart_count()
        print(f"Выйгрыш")

    def lose(self):
        self.restart_count()
        print(f"Проигрыш, значение {self.count}")

    def take_cart(self):
        pass

    def check_cart(self, count):
        print(f'значение {count}')

    # =========================================================
    def start_game(self, cards: list) -> str:
        """
        начало игры сдается 2 карты
        :param cards: массив из 2х карт
        :return: string
        """
        self.count = cards[0] + cards[1]
        return f'в руке {cards[0]} и {cards[1]}'

    def get_count(self) -> int:
        """
        получить значение счета
        :return:
        """
        return self.count

    def draw(self, card: int):
        """
        получить еще 1 карту в руку
        :param card:
        :return:
        """
        self.count = self.count + card
        return f'получено {card} в руке {self.count}'

    def restart_count(self):
        """
        сбросить счет
        :return:
        """
        self.count = 0
   # to do сделать функцию проверки, остановки очистки счета
   # создать интерфейс для записи данных