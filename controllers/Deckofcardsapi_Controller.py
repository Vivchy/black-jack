from deck.Deckofcardsapi import Deckofcardsapi
from controllers.IController import IController


class Deckofcardsapi_controller(IController):
    def __init__(self):
        self.deck = Deckofcardsapi()
        self.cart = {}

    def get_new_cart_object(self):
        """
        Получить json объект с данными карты
        :return:
        """
        self.cart = self.deck.get_cart()

    def get_value_of_cart(self):
        """
        Получить значение карты
        :return: int
        """
        self.get_new_cart_object()
        hight = {'QUEEN': 3, 'JACK': 2, 'KING': 4, 'ACE': 1}
        value = self.cart['cards'][0]['value']
        if value in hight:
            value = hight[value]
        return int(value)

    def create_deck(self):
        """
        Создать колоду
        :return: void
        """
        self.deck.get_full_deck()

    def check_number_carts(self):
        """
        Проверить колличество карт в игре
        :return: integer
        """
        return int(self.cart['remaining'])

    def get_card(self):
        return self.cart
