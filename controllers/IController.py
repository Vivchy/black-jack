from abc import ABC, abstractmethod


class IController(ABC):
    @abstractmethod
    def create_deck(self):
        """
        Создать  колоду
        :return: void
        """
        pass

    @abstractmethod
    def get_value_of_cart(self):
        """
        Получить значение карты
        :return: integer
        """
        pass

    @abstractmethod
    def check_number_carts(self):
        """
        Получить колличество карт в игре
        :return: integer
        """
        pass

    def get_card(self):
        """
        Получить данные о карте
        :return: list
        """
        pass
