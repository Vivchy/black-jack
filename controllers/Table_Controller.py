import time

from blackjack_core.bot import Bot
from blackjack_core.game import Game
from controllers.IController import IController


class Table_Controller:
    """
    Вызов функций класса во view функции возвращают информацию в текстовом виде
    для вывода в дальнейшем текта
    """

    def __init__(self, deck: IController, bot: Bot, model):
        self.deck = deck
        self.bot = bot
        self.model = model
        self.player_game = Game()

    def create_deck(self):
        self.deck.create_deck()

    def player_first_move(self):
        """
        Первый ход для игрока. Получить 2 карты
        :return: информация о имеющихся картах в руке
        """
        first_card = self.deck.get_value_of_cart()
        time.sleep(2)
        second_card = self.deck.get_value_of_cart()
        hand = self.player_game.start_game([first_card, second_card])
        # TO DO добавить модель для сохранения значения по картам
        return hand

    def player_move(self):
        """
        Взять еще 1 карту
        :return: string
        """
        card = self.deck.get_value_of_cart()
        hand = self.player_game.draw(card)
        return hand

    def bot_move(self):
        """
        Ход бота
        :return: void
        """
        resolve = self.bot.start()
        while resolve != self.bot.stop():
            card = self.deck.get_value_of_cart()
            time.sleep(3)
            resolve = self.bot.play(card)

    def get_bot_info(self):
        return self.bot.info()
